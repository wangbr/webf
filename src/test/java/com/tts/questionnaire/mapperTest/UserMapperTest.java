package com.tts.questionnaire.mapperTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.mapper.UserMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {
	
	@Autowired
	private UserMapper userMapper;
	
	/**
	 * @author:tts
	 * @description:测试 selectById
	 * @createTime 2018年4月11日 下午2:17:12
	 */
	@Test
	public void test1() {
		
		User user = userMapper.selectById("1");
		
		System.out.println("user: " + user);
	}
}
