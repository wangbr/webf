package com.tts.questionnaire.mapperTest;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.mapper.SubjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubjectMapperTest {
	
	@Autowired
	SubjectMapper subjectMapper;
	
	/**
	 * @author:tts
	 * @description:测试selectByUserId
	 * @createTime 2018年4月11日 下午3:02:28
	 */
	@Test
	public void test1() {
		
		List<Subject> subjects = subjectMapper.selectByUserId(1);
		
		for(Subject subject : subjects) {
			System.out.println("subject: " + subject);
		}
	}
	
	
}
