package com.tts.questionnaire.mapperTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tts.questionnaire.entity.ObjectiveQueAnswerChartData;
import com.tts.questionnaire.mapper.AnswerMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnswerMapperTest {
	
	@Autowired
	AnswerMapper answerMapper;
	
	/**
	 * @author:tts
	 * @description:测试submitSubject 测试提交问卷
	 * @createTime 2018年4月12日 下午7:18:18
	 */
	@Test
	public void test1() {
		try {
		
		Map<String, Object> answer1 = new HashMap<String, Object>();
		answer1.put("id", "4");
		answer1.put("answer", "1");
		
		Map<String, Object> answer2 = new HashMap<String, Object>();
		answer2.put("id", "5");
		answer2.put("answer", "2");
		
		Map<String, Object> answer3 = new HashMap<String, Object>();
		answer3.put("id", "6");
		answer3.put("answer", "哈哈哈哈");
		
		List<Map<String, Object>> answers = new ArrayList<Map<String, Object>>();
		answers.add(answer1);
		answers.add(answer2);
		answers.add(answer3);
		
		answerMapper.insertAnswers(answers);
		
		}catch(Exception exception) {
			exception.printStackTrace();
		}
	}
	
	
	/**
	 * @author:tts
	 * @description:测试获取题目的统计信息
	 * @createTime 2018年4月13日 下午12:24:42
	 */
	@Test
	public void test2() {
		try {
		List<ObjectiveQueAnswerChartData> chartDatas = answerMapper.
				getChartData(7);
		for(ObjectiveQueAnswerChartData chartData : chartDatas) {
			System.out.println("chartData: " + chartData);
		}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
	}
	
	/**
	 * @author:tts
	 * @description:获取一个主观题的所有答案
	 * @createTime 2018年4月13日 下午4:12:46
	 */
	@Test
	public void test3() {
		try {
			
			List<String> contents = answerMapper.getContentByQuestionId(6);
			for(String content : contents) {
				System.out.println("content: " + content);
			}
			
		}catch(Exception exception) {
			exception.printStackTrace();
		}
	}
}
