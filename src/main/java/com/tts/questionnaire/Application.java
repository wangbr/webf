package com.tts.questionnaire;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author:tts
 * @description:项目启动类
 * @createTime:2018年4月11日 下午1:38:39
 */
@SpringBootApplication
@MapperScan("com.tts.questionnaire.mapper")
public class Application {
    public static void main( String[] args ){
    	
    	SpringApplication.run(Application.class, args);
    	
    }
}
