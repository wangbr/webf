package com.tts.questionnaire;

import org.springframework.boot.web.servlet.FilterRegistrationBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
public class Config {
	
	  @Bean  
	  public FilterRegistrationBean filterRegist() {
	  	 UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

		 CorsConfiguration config = new CorsConfiguration();
		 config.addAllowedOrigin("*");
		 config.addAllowedHeader("*");
		 config.addAllowedMethod("*");
		 source.registerCorsConfiguration("/**", config); // CORS 配置对所有接口都有效

		 FilterRegistrationBean frBean = new FilterRegistrationBean(new CorsFilter(source));
		 frBean.setFilter(new com.tts.questionnaire.filter.LoginFilter());
		 frBean.addUrlPatterns("/*");
		 frBean.setOrder(0);
		 return frBean;
	  }


}
