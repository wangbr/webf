package com.tts.questionnaire.filter;

import java.io.IOException;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tts.questionnaire.entity.User;



public class LoginFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, 
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)servletRequest;
		HttpServletResponse response = (HttpServletResponse)servletResponse;
		String[] noFilters = {
				".html",
				".css",
				".js",
				".woff",
				
				".jsp",
				
				".mp3",
				
				".jpg",
				".png",
				".jpeg",
				
				"loginPage",
				"registerPage",
				"login",
				"register",
				
				"subjectPage",
				"/subject/submit"
			};
			
			//获取url
			String url = request.getRequestURL().toString();
			
			System.out.println("当前url: " + url);
			
			//对不需要登录的资源直接放行
			for(String noFilter : noFilters) {
				if(url.endsWith(noFilter)) {
					filterChain.doFilter(request, response);
					return;
				}
			}
			
			//判断是否登录
			User user = (User)request.getSession().getAttribute("user");
			if(user == null) {
				//重定向到登录页面
				response.sendRedirect("/loginPage");
				return;
			}
			
			filterChain.doFilter(request, response);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
