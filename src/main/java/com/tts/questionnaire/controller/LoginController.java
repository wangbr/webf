package com.tts.questionnaire.controller;


import java.util.ArrayList;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.resultSet.ResultCode;
import com.tts.questionnaire.service.LoginService;

import sun.rmi.log.LogOutputStream;

@RestController
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	/**
	 * @author:tts
	 * @description:登录
	 * @createTime 2018年4月13日 下午6:42:04
	 */
	@RequestMapping(value = "login",method = RequestMethod.POST)
	public MyResultSet<User> login(
			HttpServletRequest request,
			User user
			){
		MyResultSet<User> myResultSet = loginService.login(user);
		
		if(myResultSet.getResultCode() == null ||
				ResultCode.SUCCESS.equals(myResultSet.getResultCode())) {
			User userFromDB = myResultSet.getResultContent().get(0);
			request.getSession().setAttribute("user", userFromDB);
			myResultSet.setResultContent(new ArrayList<User>());
			myResultSet.setResultCode(ResultCode.SUCCESS);
		}
		
		return myResultSet;
	}
	
	/**
	 * @author:tts
	 * @description:登出
	 * @createTime 2018年4月16日 下午12:29:11
	 */
	@RequestMapping("/logout")
	public ModelAndView logout(HttpServletRequest request){
		
		ModelAndView mv = new ModelAndView("login");
		request.getSession().setAttribute("user", null);
		return mv;
	}
	
	/**
	 * @author:tts
	 * @description:注册
	 * @createTime 2018年4月13日 下午6:43:45
	 */
	@RequestMapping("register")
	public MyResultSet<User> register(
			User user
			){
		return loginService.register(user);
	}
	
	/**
	 * @author:tts
	 * @description:映射到登录页面
	 * @createTime 2018年4月13日 下午6:01:42
	 */
	@RequestMapping("loginPage")
	public ModelAndView loginPage() {
		return new ModelAndView("login");
	}
	
	/**
	 * @author:tts
	 * @description:映射到注册页面
	 * @createTime 2018年4月13日 下午6:22:03
	 */
	@RequestMapping("registerPage")
	public ModelAndView registerPage() {
		return new ModelAndView("register");
	}
}
