package com.tts.questionnaire.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.mapper.SubjectMapper;


@RestController
public class IndexController {
	
	@Autowired
	SubjectMapper subjectMapper;
	
	@RequestMapping("/index")
	public ModelAndView index(
			HttpServletRequest request
			) {
		
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		ModelAndView mv = new ModelAndView("subjectList");
		
		List<Subject> subjects = subjectMapper.selectByUserId(user.getId());
		
		System.out.println("subjects: " + subjects);
		
		mv.addObject("subjects", subjects);
		
		return mv;
	}
}
