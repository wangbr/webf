package com.tts.questionnaire.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tts.questionnaire.entity.Question;
import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.service.SubjectEditService;

@RestController
public class SubjectEditController {
	
	@Autowired
	SubjectEditService subjectEditService;
	
	/**
	 * @author:tts
	 * @description:修改标题
	 * @createTime 2018年4月12日 下午12:21:30
	 */
	@RequestMapping("subject/title/update")
	public MyResultSet<Subject> updateTitle(
			HttpServletRequest request,
			@RequestParam("id") Integer id,
			@RequestParam("title") String title
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		return subjectEditService.updateTitle(user, id, title);
	}
	
	/**
	 * @author:tts
	 * @description:修改内容
	 * @createTime 2018年4月12日 下午12:58:16
	 */
	@RequestMapping("subject/content/update")
	public MyResultSet<Subject> updateContent(
			HttpServletRequest request,
			@RequestParam("id") Integer id,
			@RequestParam("content") String content
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
				
		return subjectEditService.updateContent(user, id, content);
	}
	
	/**
	 * @author:tts
	 * @description:添加题目
	 * @createTime 2018年4月12日 下午2:54:23
	 */
	@RequestMapping("question/insert")
	public MyResultSet<Question> insertQuestion(
			HttpServletRequest request,
			@RequestParam(value="id", required=false) Integer id,
			@RequestParam("content") String content,
			@RequestParam("subjectId") Integer subjectId,
			@RequestParam("kind") String kind,
			@RequestParam(value="options", required=false) String optionsJson
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		//封装问题对象
		Question question = new Question();
		question.setId(id);//当前台没传入id就表示插入，传入了表示更新
		question.setKind(kind);
		question.setOptions(optionsJson);
		question.setSubject(new Subject(subjectId));
		question.setContent(content);
		
		System.out.println("question: " + question);
		
		subjectEditService.insertQuestion(user, question);
		
		return new MyResultSet<Question>();
	}
	
	
	/**
	 * @author:tts
	 * @description:删除问题
	 * @createTime 2018年4月12日 下午9:15:19
	 */
	@RequestMapping("question/delete")
	public MyResultSet<Question> deleteQuestion(
			HttpServletRequest request,
			@RequestParam("id") Integer id
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
				
		return subjectEditService.deleteQuestion(user, id);
	}
}
