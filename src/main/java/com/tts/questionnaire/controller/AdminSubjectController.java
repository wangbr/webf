package com.tts.questionnaire.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.OBJECT_NOT_EXIST;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.mapper.SubjectMapper;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.service.SubjectService;

@RestController
public class AdminSubjectController {
	
	@Autowired
	SubjectService subjectService;
	
	/**
	 * @author:tts
	 * @description:获取统计信息
	 * @createTime 2018年4月13日 下午12:36:01
	 */
	@RequestMapping("details/{id}")
	public MyResultSet<Subject> getChartData(
			HttpServletRequest request,
			@PathVariable("id") Integer subjectId
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		
		return subjectService.getSubjectWithChartInfo(user, subjectId);
	}
	
	/**
	 * @author:tts
	 * @description:删除问卷
	 * @createTime 2018年4月13日 下午4:48:41
	 */
	@RequestMapping("subject/delete/{id}")
	public MyResultSet<Subject> deleteSubject(
			HttpServletRequest request,
			@PathVariable("id") Integer subjectId
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		return subjectService.deleteSubject(user, subjectId);
	}
	
	
	/**
	 * @author:tts
	 * @description:发布问卷
	 * @createTime 2018年4月12日 下午7:57:05
	 */
	@RequestMapping("subject/publish/{id}")
	public MyResultSet<Subject> publishSubject(
			HttpServletRequest request,
			@PathVariable("id") Integer id
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		return subjectService.publishSubject(user, id);
	}
	
	/**
	 * @author:tts
	 * @description:下架问卷
	 * @createTime 2018年4月12日 下午7:57:15
	 */
	@RequestMapping("subject/remove/{id}")
	public MyResultSet<Subject> removeSubject(
			HttpServletRequest request,
			@PathVariable("id") Integer id
			){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		return subjectService.removeSubject(user, id);
	}
	
	/**
	 * @author:tts
	 * @description:获取问卷资源
	 * @createTime 2018年4月12日 上午9:38:36
	 */
	@RequestMapping("subject/{id}")
	public MyResultSet<Subject> getSubject(
			HttpServletRequest request,
			@PathVariable("id") Integer id
			) {
		MyResultSet<Subject> myResultSet = subjectService.getSubjectById(id);
		
		//获取sessionId
		String sessionId = request.getSession().getId();
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("token", sessionId);
		
		myResultSet.setExtra(res);
		
		return myResultSet;
	}
	
	/**
	 * @author:tts
	 * @description:添加一个空白的问卷
	 * @createTime 2018年4月12日 上午10:07:32
	 */
	@RequestMapping("subject/add")
	public MyResultSet<Subject> newSubject(HttpServletRequest request){
		//获取user
		User user = (User)request.getSession().getAttribute("user");
		
		return subjectService.newBlankSubject(user);
	}
	
	/**
	 * @author:tts
	 * @description:映射到编辑界面
	 * @createTime 2018年4月12日 上午11:16:53
	 */
	@RequestMapping("editPage")
	public ModelAndView editPage(
			) {
		return new ModelAndView("edit");
	}
	
	/**
	 * @author:tts
	 * @description:映射到问卷页面
	 * @createTime 2018年4月12日 下午3:15:03
	 */
	@RequestMapping("subjectPage")
	public ModelAndView subjectPage() {
		return new ModelAndView("subject");
	}
	
	/**
	 * @author:tts
	 * @description:映射到统计界面
	 * @createTime 2018年4月13日 上午10:20:09
	 */
	@RequestMapping("detailsPage")
	public ModelAndView detailspage() {
		return new ModelAndView("details");
	}
}
