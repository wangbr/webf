package com.tts.questionnaire.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.service.SubjectService;

@RestController
public class CutomerSubjectController {
	
	@Autowired
	SubjectService subjectService;
	
	/**
	 * @author:tts
	 * @description:提交问卷
	 * @createTime 2018年4月12日 下午6:36:42
	 */
	@RequestMapping("/subject/submit")
	public MyResultSet<Subject> submitSubject(
			@RequestBody Map<String, Object> map
			){
		//获取答案数组
		List<Map<String, Object>> answers = (List<Map<String, Object>>)
				map.get("answers");
		
		//获取userTag
		String userTag = (String)answers.get(0).get("userTag");
		
		return subjectService.submitSubject(userTag, answers);
	}
}
