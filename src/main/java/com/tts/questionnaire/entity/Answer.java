package com.tts.questionnaire.entity;

/**
 * @author:tts
 * @description:答案的实体类
 * @createTime:2018年4月11日 下午2:06:09
 */
public class Answer {
	
	private Integer id;
	
	private Question question; 
	
	private String content;
	
	private String userTag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserTag() {
		return userTag;
	}

	public void setUserTag(String userTag) {
		this.userTag = userTag;
	}

	@Override
	public String toString() {
		return "Answer [id=" + id + ", question=" + question + ", content=" + content + ", userTag=" + userTag + "]";
	}
	
	
	
}
