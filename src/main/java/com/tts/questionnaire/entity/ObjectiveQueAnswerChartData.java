package com.tts.questionnaire.entity;


/**
 * @author:tts
 * @description:选择题的每个选项的统计信息
 * @createTime:2018年4月13日 下午12:01:40
 */
public class ObjectiveQueAnswerChartData {
	
	//选项的索引，0表示A, 1表示B
	private String content;
	
	//某个选项选择的人数
	private Integer count;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "ObjectiveQueAnswerChartData [content=" + content + ", count=" + count + "]";
	}

}
