package com.tts.questionnaire.entity;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;


public class Question {
	
	public static final String KIND_SUBJECTIVE = "subjective";
	
	public static final String KIND_OBJECTIVE = "objective";
	
	
	private Integer id;
	
	private Subject subject;
	
	private String content;
	private String kind;
	private String options;
	
	private Map<String, Object> chartDatas;//客观观题的统计数据
	
	private List<String> answersContent;//主观题直接显示所有回答
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public Map<String, Object> getChartDatas() {
		return chartDatas;
	}
	public void setChartDatas(Map<String, Object> chartDatas) {
		this.chartDatas = chartDatas;
	}
	
	public List<String> getAnswersContent() {
		return answersContent;
	}
	public void setAnswersContent(List<String> answerContent) {
		this.answersContent = answerContent;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", subject=" + subject + ", content=" + content + ", kind=" + kind + ", options="
				+ options + ", chartDatas=" + chartDatas + ", answersContent=" + answersContent + "]";
	}
	
	
}
