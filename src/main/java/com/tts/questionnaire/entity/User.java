package com.tts.questionnaire.entity;

import java.io.Serializable;

/**
 * @author:tts
 * @description:用户表
 * @createTime:2018年4月11日 下午1:58:11
 */
public class User implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Integer id;
	private String username;
	private String pwd;
	
	public User() {
		
	}
	
	public User(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", pwd=" + pwd + "]";
	}
}
