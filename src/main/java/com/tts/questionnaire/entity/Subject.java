package com.tts.questionnaire.entity;

import java.util.List;

/**
 * @author:tts
 * @description:问卷实体类
 * @createTime:2018年4月11日 下午1:59:56
 */
public class Subject {
	
	private Integer id;
	private User user;
	
	private String title;
	private String content;
	private Boolean status;
	private String createDate;
	
	private Integer submitCount;
	
	private List<Question> questions;
	
	public Subject() {
		
	}
	
	public Subject(Integer id) {
		this.id = id;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Integer getSubmitCount() {
		return submitCount;
	}

	public void setSubmitCount(Integer submitCount) {
		this.submitCount = submitCount;
	}

	@Override
	public String toString() {
		return "Subject [id=" + id + ", user=" + user + ", title=" + title + ", content=" + content + ", status="
				+ status + ", createDate=" + createDate + ", submitCount=" + submitCount + ", questions=" + questions
				+ "]";
	}
	
}
