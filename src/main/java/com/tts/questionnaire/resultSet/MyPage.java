package com.tts.questionnaire.resultSet;

/*
 * createTime: 2018-1-26 17:02
 * 封装的分页类
 */
public class MyPage {
	private Long total;//总的数据的条数
	
	private Integer totalPage;//总的页数
	
	private Integer currentPage;//当前页码
	
	private Integer pageSize;//一页包含的数据条数
	
	public MyPage() {
		
	}

	

	public Long getTotal() {
		return total;
	}



	public void setTotal(Long total) {
		this.total = total;
	}



	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}



	@Override
	public String toString() {
		return "Page [total=" + total + ", totalPage=" + totalPage + ", currentPage=" + currentPage + ", pageSize="
				+ pageSize + "]";
	}

	
}
