package com.tts.questionnaire.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.tts.questionnaire.entity.Subject;


public interface SubjectMapper {
	
	@Delete("DELETE FROM tb_subject WHERE id = #{id} AND user_id = #{userId}")
	public Long deleteSubject(
			@Param("userId") Integer userId,
			@Param("id") Integer id);
	
	
	@Update("UPDATE tb_subject "
			+ "SET submit_count = submit_count + #{increaseCount} "
			+ "WHERE id = #{id}")
	public Long increaseSubmitCount(
			@Param("increaseCount") Integer increaseCount,
			@Param("id") Integer id
			);
	
	//更新状态
	@Update("UPDATE tb_subject SET status = #{status} "
			+ "WHERE id = #{id} AND user_id = #{userId}")
	public Long updateStatus(
			@Param("userId") Integer userId,
			@Param("id") Integer id,
			@Param("status") Boolean status
		);
	
	
	//修改content
	@Update("UPDATE tb_subject "
			+ "SET content = #{content} "
			+ "WHERE id = #{id} AND user_id = #{userId}")
	public Long updateContent(
			@Param("userId") Integer userId,
			@Param("id") Integer id,
			@Param("content") String content
		);
	
	
	//修改标题
	@Update("UPDATE tb_subject "
			+ "SET title = #{title} "
			+ "WHERE id = #{id} AND user_id = #{userId}")
	public Long updateTitle(
			@Param("userId") Integer userId,
			@Param("id") Integer id,
			@Param("title") String title
		);
	
	
	@Insert("INSERT INTO tb_subject SET user_id = #{userId}, status = false")
	public Long insert(@Param("userId") Integer userId);
	
	public List<Subject> selectByUserId(@Param("userId") Integer userId);
	
	public Subject selectById(@Param("id") Integer id);
	
}
