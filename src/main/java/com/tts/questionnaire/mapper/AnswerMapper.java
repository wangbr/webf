package com.tts.questionnaire.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.dao.DataAccessException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.tts.questionnaire.entity.Answer;
import com.tts.questionnaire.entity.ObjectiveQueAnswerChartData;



public interface AnswerMapper {
	
	/**
	 * @author:tts
	 * @description:获取主观题的所有答案
	 * @createTime 2018年4月13日 下午4:10:56
	 */
	@Select("SELECT content FROM tb_answer WHERE question_id = #{questionId}")
	public List<String> getContentByQuestionId(
			@Param("questionId") Integer questionId
			);
	
	/**
	 * @author:tts
	 * @description:获取该问题的统计信息
	 * @createTime 2018年4月13日 下午12:05:06
	 */
	public List<ObjectiveQueAnswerChartData> getChartData(
			@Param("questionId") Integer questionId
			);
	
	/**
	 * @author:tts
	 * @description:批量插入answer
	 * @createTime 2018年4月12日 下午7:04:02
	 */
	public Long insertAnswers(
			@Param("answers") List<Map<String, Object>> answers
		)throws DataAccessException;
}
