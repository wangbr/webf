package com.tts.questionnaire.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.tts.questionnaire.entity.Question;


public interface QuestionMapper {
	
	@Delete("DELETE FROM tb_question WHERE id = #{id}")
	public Long delete(@Param("id") Integer id);
	
	@Update("UPDATE tb_question "
			+ "SET content = #{content}, options = #{options} "
			+ "WHERE id = #{id}" )
	public Long update(Question question);
	
	
	@Insert("INSERT INTO tb_question "
			+ "SET subject_id = #{subject.id}, "
			+ "content = #{content}, "
			+ "options = #{options}, "
			+ "kind = #{kind}")
	public Long insert(Question question);
	
	
	
	@Select("SELECT id, subject_id, content, options, kind "
			+ "FROM tb_question WHERE subject_id = #{subjectId}")
	public List<Question> selectBySubjectId(
			@Param("subjectId") String subjectId);
}
