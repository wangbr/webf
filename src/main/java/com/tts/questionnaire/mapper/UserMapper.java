package com.tts.questionnaire.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.tts.questionnaire.entity.User;

/**
 * @author:tts
 * @description:
 * @createTime:2018年4月11日 下午2:09:50
 */
public interface UserMapper {
	
	@Select("SELECT id, username, pwd "
			+ "FROM tb_user "
			+ "WHERE username = #{username}")
	public User selectByUsername(@Param("username") String username);
	
	@Insert("INSERT INTO "
			+ "tb_user(username, pwd) "
			+ "VALUES(#{username}, #{pwd})")
	public Long insert(User user);
	
	@Select("SELECT id, username, pwd FROM tb_user WHERE id = #{id}")
	public User selectById(@Param("id") String id);
}
