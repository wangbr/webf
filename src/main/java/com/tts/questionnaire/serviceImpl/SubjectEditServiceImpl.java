package com.tts.questionnaire.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tts.questionnaire.entity.Question;
import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.mapper.QuestionMapper;
import com.tts.questionnaire.mapper.SubjectMapper;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.service.SubjectEditService;

@Service
public class SubjectEditServiceImpl implements SubjectEditService{
	
	@Autowired
	SubjectMapper subjectMapper;
	
	@Autowired
	QuestionMapper questionMapper;

	@Override
	public MyResultSet<Subject> updateTitle(User user, 
			Integer id, String title) {
		
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		subjectMapper.updateTitle(user.getId(), id, title);
		
		return myResultSet;
	}

	@Override
	public MyResultSet<Subject> updateContent(User user, 
			Integer id, String content) {
		
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		subjectMapper.updateContent(user.getId(), id, content);
		
		return myResultSet;
	}

	@Override
	public MyResultSet<Question> insertQuestion(User user, Question question) {
		
		MyResultSet<Question> myResultSet = new MyResultSet<Question>();
		
		//为空表示插入
		if(question.getId() == null) {
			questionMapper.insert(question);
		}
		//不为空表示更新
		else {
			questionMapper.update(question);
		}
		
		return myResultSet;
	}

	@Override
	public MyResultSet<Question> deleteQuestion(User user, Integer id) {
		MyResultSet<Question> myResultSet = new MyResultSet<Question>();
		
		questionMapper.delete(id);
		
		return myResultSet;
	}



}
