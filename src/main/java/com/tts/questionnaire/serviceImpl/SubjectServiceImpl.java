package com.tts.questionnaire.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.fabric.xmlrpc.base.Array;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.tts.questionnaire.entity.ObjectiveQueAnswerChartData;
import com.tts.questionnaire.entity.Question;
import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.mapper.AnswerMapper;
import com.tts.questionnaire.mapper.QuestionMapper;
import com.tts.questionnaire.mapper.SubjectMapper;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.resultSet.ResultCode;
import com.tts.questionnaire.service.SubjectService;

@Service
public class SubjectServiceImpl implements SubjectService{
	
	@Autowired
	SubjectMapper subjectMapper;
	
	@Autowired
	AnswerMapper answerMapper;
	
	@Autowired
	QuestionMapper questionMapper;
	
	@Override
	public MyResultSet<Subject> getSubjectById(Integer id) {
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		Subject subject = subjectMapper.selectById(id);
		
		myResultSet.setResultContent(subject);
		
		return myResultSet;
	}

	/**
	 * 增加一个空白的问卷
	 */
	@Override
	public MyResultSet<Subject> newBlankSubject(User user) {
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		//添加
		subjectMapper.insert(user.getId());
		
		return myResultSet;
	}

	/**
	 * 提交问卷
	 */
	@Override
	@Transactional
	public MyResultSet<Subject> submitSubject(
			String userTag,
			List<Map<String, Object>> answers) {
		
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		//获取答案对应的问卷的id
		Integer subjectId = (Integer)answers.get(0).get("subjectId");
		
		try {
			//添加 
			answerMapper.insertAnswers(answers);
		}catch(DataAccessException e) {
			final Throwable cause = e.getCause();
			if( cause instanceof MySQLIntegrityConstraintViolationException ) {
				System.out.println("不能重复提交！");
				myResultSet.setResultCode(ResultCode.BAD_REQUEST);
				myResultSet.setResultInfo("不能重复提交");
				return myResultSet;
			}
			myResultSet.setResultCode(ResultCode.BAD_REQUEST);
			myResultSet.setResultInfo("数据访问错误");
		}
		
		//增加填写问卷的人数
		subjectMapper.increaseSubmitCount(1, subjectId);
		
		return myResultSet;
	}

	/**
	 * 发布问卷
	 */
	@Override
	public MyResultSet<Subject> publishSubject(User user, Integer subjectId) {
		
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		//修改状态
		subjectMapper.updateStatus(user.getId(), subjectId, true);
		
		return myResultSet;
	}

	/**
	 * 下架问卷
	 */
	@Override
	public MyResultSet<Subject> removeSubject(User user, Integer subjectId) {
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		//修改状态
		subjectMapper.updateStatus(user.getId(), subjectId, false);
		
		return myResultSet;
	}

	/**
	 * @author:tts
	 * @description:查出问卷信息，并附带统计信息
	 * @createTime 2018年4月13日 上午11:41:45
	 */
	@Override
	public MyResultSet<Subject> getSubjectWithChartInfo(User user, Integer subjectId) {
		
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		//先查出问卷基本信息
		Subject subject = subjectMapper.selectById(subjectId);
		
		//遍历问卷的题目
		for(Question question : subject.getQuestions()) {
			//如果是客观题
			if(Question.KIND_OBJECTIVE.equals(question.getKind())) {
				//获取每个客观题的统计信息
				List<ObjectiveQueAnswerChartData> chartDatas = 
						answerMapper.getChartData(question.getId());
				//将统计信息由list转化为map
				Map<String, Object> chartDatasMap = new HashMap<String, Object>();
				for(ObjectiveQueAnswerChartData chartData : chartDatas) {
					chartDatasMap.put(chartData.getContent(), 
							chartData.getCount());
				}
				question.setChartDatas(chartDatasMap);
				
				//为chartsDatas设置空数组
				/*question.setAnswerContent(new ArrayList<>());*/
			}
			//如果是主观题
			if(Question.KIND_SUBJECTIVE.equals(question.getKind())) {
				
				question.setAnswersContent(answerMapper.getContentByQuestionId(
						question.getId()));
				
				//为chartDatas设置空map
				/*question.setChartDatas(new HashMap<String, Object>());*/
			}
			
		}
		
		myResultSet.setResultContent(subject);
		
		return myResultSet;		
	}

	@Override
	public MyResultSet<Subject> deleteSubject(User user, Integer subjectId) {
		MyResultSet<Subject> myResultSet = new MyResultSet<Subject>();
		
		//修改状态
		subjectMapper.deleteSubject(user.getId(), subjectId);
		
		return myResultSet;
	}

}
