package com.tts.questionnaire.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.mapper.UserMapper;
import com.tts.questionnaire.resultSet.MyResultSet;
import com.tts.questionnaire.resultSet.ResultCode;
import com.tts.questionnaire.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	UserMapper userMapper;

	@Override
	public MyResultSet<User> login(User user) {
		
		MyResultSet<User> myResultSet = new MyResultSet<User>();
		
		//查出username的记录
		User userFromDB = userMapper.selectByUsername(user.getUsername());
		
		if(userFromDB == null || !userFromDB.getPwd().equals(user.getPwd())) {
			System.out.println("验证错误！");
			myResultSet.setResultCode(ResultCode.BAD_REQUEST);
			myResultSet.setResultInfo("用户名或密码错误");
			return myResultSet;
		}
		
		
		myResultSet.setResultContent(userFromDB);
		
		return myResultSet;
	}

	@Override
	public MyResultSet<User> register(User user) {
		MyResultSet<User> myResultSet = new MyResultSet<User>();
		
		//插入
		userMapper.insert(user);
		
		return myResultSet;
	}

}
