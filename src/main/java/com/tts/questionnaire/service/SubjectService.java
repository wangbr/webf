package com.tts.questionnaire.service;


import java.util.List;
import java.util.Map;

import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.resultSet.MyResultSet;


public interface SubjectService {
	
	public MyResultSet<Subject> deleteSubject(User user, Integer subjectId);
	
	/**
	 * @author:tts
	 * @description:查询问卷信息并附带统计信息
	 * @createTime 2018年4月13日 上午11:41:11
	 */
	public MyResultSet<Subject> getSubjectWithChartInfo(User user, 
			Integer subjectId);
	
	public MyResultSet<Subject> publishSubject(User user, Integer subjectId);
	
	public MyResultSet<Subject> removeSubject(User user, Integer subjectId);
	
	public MyResultSet<Subject> getSubjectById(Integer id);
	
	
	public MyResultSet<Subject> newBlankSubject(User user);
	
	/**
	 * @author:tts
	 * @description:提交问卷,userTag不一定是用户名，只是提交问卷的一个标识，
	 * 可以使jsessionId,用户名,ip地址,mac地址
	 * @createTime 2018年4月12日 下午7:13:32
	 */
	public MyResultSet<Subject> submitSubject(String userTag,
			List<Map<String, Object>> answers);
}
