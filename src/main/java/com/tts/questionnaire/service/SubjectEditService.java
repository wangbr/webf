package com.tts.questionnaire.service;

import com.tts.questionnaire.entity.Question;
import com.tts.questionnaire.entity.Subject;
import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.resultSet.MyResultSet;

public interface SubjectEditService {
	
	public MyResultSet<Question> deleteQuestion(User user, Integer id);
	
	public MyResultSet<Question> insertQuestion(User user, Question question);
	
	public MyResultSet<Subject> updateTitle(
			User user, Integer id, String title);
	
	public MyResultSet<Subject> updateContent(
			User user, Integer id, String content
			);
}
