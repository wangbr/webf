package com.tts.questionnaire.service;

import com.tts.questionnaire.entity.User;
import com.tts.questionnaire.resultSet.MyResultSet;

public interface LoginService {
	
	public MyResultSet<User> login(User user);
	
	public MyResultSet<User> register(User user);
	
}
