$(document).ready(function(){

  var myPlaylist = new jPlayerPlaylist({
    jPlayer: "#jplayer_N",
    cssSelectorAncestor: "#jp_container_N"
  }, [
    {
      id:"1",
      title:"红豆",
      artist:"王菲",
      mp3:"/song/红豆.mp3",
      poster: "images/m0.jpg"
    },
    {
      id:"2",
      title:"红玫瑰",
      artist:"王菲 陈奕迅",
      mp3:"/song/红玫瑰.mp3",
      poster: "images/m0.jpg"
    }
  ], {
    playlistOptions: {
      enableRemoveControls: true,
      autoPlay: false
    },
    swfPath: "js/jPlayer",
    supplied: "webmv, ogv, m4v, oga, mp3",
    smoothPlayBar: true,
    keyEnabled: true,
    audioFullScreen: false
  });
  
  $(document).on($.jPlayer.event.pause, myPlaylist.cssSelector.jPlayer,  function(){
    $('.musicbar').removeClass('animate');
    $('.jp-play-me').removeClass('active');
    $('.jp-play-me').parent('li').removeClass('active');
  });

  $(document).on($.jPlayer.event.play, myPlaylist.cssSelector.jPlayer,  function(){
    $('.musicbar').addClass('animate');
  });

  $(document).on('click', '.jp-play-me', function(e){
    e && e.preventDefault();
    var $this = $(e.target);
    if (!$this.is('a')) $this = $this.closest('a');

    $('.jp-play-me').not($this).removeClass('active');
    $('.jp-play-me').parent('li').not($this.parent('li')).removeClass('active');

    $this.toggleClass('active');
    $this.parent('li').toggleClass('active');
    if( !$this.hasClass('active') ){
      myPlaylist.pause();
    }else{
      var i = Math.floor(Math.random() * (1 + 7 - 1));
      myPlaylist.play(i);
    }
    
  });
  
  /*****点击歌曲 开始播放*****/
  $(".play-music").click(function(){
	  var id = $(this).attr("musicId");
	  var title = $(this).attr("name");
	  var artist = $(this).attr("singer");
	  var mp3 = $(this).attr("location");
	  var poster = $(this).attr("imgLocation");
	  
	  var song = {
	     id:id,
		 title:title,
		 artist:artist,
		 mp3:mp3,
		 poster:poster
	  }
	  
	  //查找列表，看是否已有此歌曲
	  var searchIndex = -1;
	  $(myPlaylist.playlist).each(function(index, element){
		  if(element.id == song.id){
			  searchIndex = index;
			  return false;
		  }
	  });
	  if(searchIndex != -1){
		  myPlaylist.play(searchIndex);
		  return false;
	  }
	  
	  myPlaylist.add(song);
	  myPlaylist.play(myPlaylist.playlist.length - 1);
  });
  
  myPlaylist.pause();

});