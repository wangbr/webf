//问卷统计信息
var mainApp = angular.module("mainApp", []);

mainApp.controller("mainController", function($scope, $http){
	
	//从url中获取参数
	$scope.subject = {
		id:getQueryString("subjectId")
	};
	
	
	$scope.getChooseCount = function(){
		var count = this.question.chartDatas[this.$index];
		if(typeof count == "undefined"){
			count = 0;
		}
		return count + "人选择";
	}
	
	//获取带统计信息的问卷信息
	//获取subject
	$http({
	    method: 'POST',
	    url: '/details/' + $scope.subject.id
	}).then(function success(response) {
		$scope.subject = response.data.resultContent[0];
		
		//将json格式的问题选项转化为对象格式
		$($scope.subject.questions).each(function(i, que){
			que.optionsArray = JSON.parse(que.options);
			if(que.kind == "objective"){
				que.kindString = "客观题";
			}
			else{
				que.kindString = "主观题";
			}
		})
		
		setTimeout(function(){
			updateCharts();
		}, 500)
	}, function error(response) {
	      
	});
	
	function updateCharts(){
		//遍历问卷答案的统计信息
		$($scope.subject.questions).each(function(i, question){
			var chartId = "echart_" + question.id;
			var chartDiv = document.getElementById(chartId)
			var myChart = echarts.init(chartDiv);
			//构造数据
			var datas = new Array();
			$(question.optionsArray).each(function(j, option){
				//计算value
				var value = question.chartDatas[j];
				if(typeof value == "undefined"){
					value = 0;
				}
				//计算name,需要计算百分比
				var total = $scope.subject.submitCount;
				var percentage = ((value / total) * 100).toFixed(2);
				console.log("percentage: " + percentage)
				var name = option.title + " " + percentage + "%";
				
				var data = {
					name:name,
					value:value
				}
				datas.push(data);
			})
			myChart.setOption({
			    series : [
			        {
			            name: '访问来源',
			            type: 'pie',
			            radius: '55%',
			            data:datas
			        }
			    ]
			})
		})
	}
	
})

//获取url中的参数
function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); 
    return null; 
 } 