//******编辑的controller******//

var mainApp = angular.module("mainApp", []);


//全局的controller
mainApp.controller("mainController", function($scope, $http){
	
	//从url中获取参数
	$scope.subject = {
		id:getQueryString("subjectId")
	};
	
	$scope.editQue = {}
	
	$scope.editQue.optionsArray = new Array();
	
	//删除问卷
	$scope.questionDelete = function(){
		$http({
		    method: 'POST',
		    url: "/question/delete",
		    params:{
		    	id:this.question.id
		    }
		}).then(function success(response) {
			
			var resultSet = response.data;
			if(resultSet.resultCode == 200 || resultSet.resultCode == null){
				alert("删除成功！");
				window.location.reload();
			}
			else{
				alert("删除失败！");
			}
			
		}, function error(response) {
		      alert("系统错误！");
		});
	}
	
	//修改问卷
	$scope.questionUpdate = function(){
		var question = this.question;
		//客观题
		if(question.kind == "objective"){
			$scope.editQue = question;
		}
		//主观题
		else{
			$scope.subjectiveQue = question;
		}
	}
	
	//添加选项
	$scope.addOption = function(){
		this.editQue.optionsArray.push({
			index:$scope.editQue.optionsArray.length
		})
	}
	
	//删除选项
	$scope.deleteOption = function(){
		var index = this.option.index;
		$scope.editQue.optionsArray.splice(index--, 1);
	}
	
	//提交问卷标题
	$scope.submitTitle = function(){
		$http({
		    method: 'POST',
		    url: "/subject/title/update",
		    params:{
		    	id:$scope.subject.id,
		    	title:$scope.subject.title
		    }
		}).then(function success(response) {
			
			var resultSet = response.data;
			if(resultSet.resultCode == 200 || resultSet.resultCode == null){
				alert("提交成功！");
				window.location.reload();
			}
			else{
				alert("提交失败！");
			}
			
		}, function error(response) {
		      alert("系统错误！");
		});
	}
	
	//提交问卷提示内容
	$scope.submitContent = function(){
		$http({
		    method: 'POST',
		    url: "/subject/content/update",
		    params:{
		    	id:$scope.subject.id,
		    	content:$scope.subject.content
		    }
		}).then(function success(response) {
			
			var resultSet = response.data;
			if(resultSet.resultCode == 200 || resultSet.resultCode == null){
				alert("提交成功！");
				window.location.reload();
			}
			else{
				alert("提交失败！");
			}
			
		}, function error(response) {
		      alert("系统错误！");
		});
	}
	
	//提交问卷客观题
	$scope.submitQue = function(){
		var editQue = $scope.editQue;
		$http({
		    method: 'POST',
		    url: "/question/insert",
		    params:{
		    	id:editQue.id,
		    	subjectId:$scope.subject.id,
		    	kind:'objective',
		    	content:editQue.content,
		    	options:JSON.stringify(editQue.optionsArray)
		    }
		}).then(function success(response) {
			
			var resultSet = response.data;
			if(resultSet.resultCode == 200 || resultSet.resultCode == null){
				alert("提交成功！");
				window.location.reload();
				
			}
			else{
				alert("提交失败！");
			}
			
		}, function error(response) {
		      alert("系统错误！");
		});
	}
	
	//提交问卷主观题
	$scope.submitSubjectiveQue = function(){
		var editQue = $scope.subjectiveQue;
		
		$http({
		    method: 'POST',
		    url: "/question/insert",
		    params:{
		    	id:editQue.id,
		    	subjectId:$scope.subject.id,
		    	kind:'subjective',
		    	content:editQue.content
		    }
		}).then(function success(response) {
			
			var resultSet = response.data;
			if(resultSet.resultCode == 200 || resultSet.resultCode == null){
				alert("提交成功！");
				window.location.reload();
			}
			else{
				alert("提交失败！");
			}
			
		}, function error(response) {
		      alert("系统错误！");
		});
	}
	
	
	//问卷预览
	$scope.preview = function(){
		window.location = "/subjectPage?subjectId=" + $scope.subject.id;
	}
	
	//获取subject
	$http({
	    method: 'POST',
	    url: '/subject/' + $scope.subject.id
	}).then(function success(response) {
		$scope.subject = response.data.resultContent[0];
		
		//将json格式的问题选项转化为对象格式
		$($scope.subject.questions).each(function(i, que){
			que.optionsArray = JSON.parse(que.options);
			if(que.kind == "objective"){
				que.kindString = "客观题";
			}
			else{
				que.kindString = "主观题"
			}
		})
	}, function error(response) {
	      
	});
	
})


//获取url中的参数
function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); 
    return null; 
 } 
