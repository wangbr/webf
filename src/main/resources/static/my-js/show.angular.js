//问卷显示
var mainApp = angular.module("mainApp", []);

mainApp.controller("mainController", function($scope, $http){
	
	//从url中获取参数
	$scope.subject = {
		id:getQueryString("subjectId")
	};
	
	//获取问卷信息
	//获取subject
	$http({
	    method: 'POST',
	    url: '/subject/' + $scope.subject.id
	}).then(function success(response) {
		//获取tooken
		var tooken = response.data.extra.token;
		$scope.key = "questionaire_token_" + $scope.subject.id
		//从localStorge
		var tokenFromLocal = localStorage.getItem($scope.key);
		
		if(tokenFromLocal == null || tokenFromLocal == ""){
			localStorage.setItem($scope.key, tooken);
		}
		
		console.log("tokenFromLocal: " + localStorage.getItem($scope.key));
		
		$scope.subject = response.data.resultContent[0];
		
		//将json格式的问题选项转化为对象格式
		$($scope.subject.questions).each(function(i, que){
			que.optionsArray = JSON.parse(que.options);
			if(que.kind == "objective"){
				que.kindString = "客观题";
			}
			else{
				que.kindString = "主观题"
			}
		})
	}, function error(response) {
	      
	});
	
	//提交问卷
	$scope.submitSubject = function(){
		var subject = $scope.subject;
		var answers = new Array();
		
		//从本地获取tooken
		var tooken = localStorage.getItem($scope.key);
		
		$(subject.questions).each(function(i, que){
			answers.push({
				userTag:tooken + "_" + que.id,
				subjectId:subject.id,
				id:que.id,
				answer:que.answer
			});
		})
		
		var ifComplete = true;
		$(answers).each(function(i, answer){
			if(answer.answer == null || answer.answer == ""){
				alert("请填写完整!");
				ifComplete = false; 
				return false;
			}
		})
		if(!ifComplete){
			return false;
		}
		
		$http({
		    method: 'POST',
		    url: '/subject/submit',
		    data:{
		    	answers:answers
		    }
		}).then(function success(response) {
			
			var resultSet = response.data;
			if(resultSet.resultCode == 200 || resultSet.resultCode == null){
				alert("提交成功！");
				window.location.href="about:blank";
				window.close();
			}
			else{
				alert("提交失败！" + resultSet.resultInfo);
				window.location.href="about:blank";
				window.close();
			}
			
		}, function error(response) {
		      alert("系统错误");
		});
	}
})

//获取url中的参数
function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); 
    return null; 
 } 