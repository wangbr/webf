/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : questionnaire

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-07-16 15:59:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_answer
-- ----------------------------
DROP TABLE IF EXISTS `tb_answer`;
CREATE TABLE `tb_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `question_id` int(11) DEFAULT NULL COMMENT '关联question表的主键',
  `content` varchar(400) DEFAULT NULL COMMENT '答案',
  `user_tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_tag` (`user_tag`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `tb_answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `tb_question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COMMENT='问题答案表';

-- ----------------------------
-- Records of tb_answer
-- ----------------------------
INSERT INTO `tb_answer` VALUES ('130', '18', '0', 'B6E2EFA9A0335DBD2116AFD5CE3E53A7_18');
INSERT INTO `tb_answer` VALUES ('131', '19', '1', 'B6E2EFA9A0335DBD2116AFD5CE3E53A7_19');
INSERT INTO `tb_answer` VALUES ('132', '20', '好好站岗', 'B6E2EFA9A0335DBD2116AFD5CE3E53A7_20');
INSERT INTO `tb_answer` VALUES ('133', '18', '0', 'C87EE230A6F9C95E8EFC5F7D8B648745_18');
INSERT INTO `tb_answer` VALUES ('134', '19', '1', 'C87EE230A6F9C95E8EFC5F7D8B648745_19');
INSERT INTO `tb_answer` VALUES ('135', '20', '好机会', 'C87EE230A6F9C95E8EFC5F7D8B648745_20');
INSERT INTO `tb_answer` VALUES ('136', '21', '0', 'E6F4D5E00863CFB734CB5C808AE8DAC3_21');
INSERT INTO `tb_answer` VALUES ('137', '22', '加强巡逻', 'E6F4D5E00863CFB734CB5C808AE8DAC3_22');
INSERT INTO `tb_answer` VALUES ('138', '15', '0', 'CB5EA35F6ED386CFAA13D29646C1B340_15');
INSERT INTO `tb_answer` VALUES ('139', '16', '1', 'CB5EA35F6ED386CFAA13D29646C1B340_16');
INSERT INTO `tb_answer` VALUES ('140', '17', '很有用', 'CB5EA35F6ED386CFAA13D29646C1B340_17');
INSERT INTO `tb_answer` VALUES ('141', '15', '0', 'D75ADF0D2EF65797A7F1E997EF6F2578_15');
INSERT INTO `tb_answer` VALUES ('142', '16', '1', 'D75ADF0D2EF65797A7F1E997EF6F2578_16');
INSERT INTO `tb_answer` VALUES ('143', '17', '还行', 'D75ADF0D2EF65797A7F1E997EF6F2578_17');
INSERT INTO `tb_answer` VALUES ('150', '21', '0', '567BD9D9377645D7BBC13B863626E6B9_21');
INSERT INTO `tb_answer` VALUES ('151', '22', 'www', '567BD9D9377645D7BBC13B863626E6B9_22');

-- ----------------------------
-- Table structure for tb_question
-- ----------------------------
DROP TABLE IF EXISTS `tb_question`;
CREATE TABLE `tb_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `subject_id` int(11) DEFAULT NULL COMMENT '关联subject表的主键',
  `content` varchar(200) DEFAULT NULL COMMENT '题目的题干',
  `kind` enum('subjective','objective') DEFAULT NULL COMMENT '题目的类型，主观题还是客观题',
  `options` varchar(1000) DEFAULT NULL COMMENT '题目的选项,是一个json字符串',
  PRIMARY KEY (`id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `tb_question_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `tb_subject` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='问卷的问题表';

-- ----------------------------
-- Records of tb_question
-- ----------------------------
INSERT INTO `tb_question` VALUES ('4', '1', '你最近感到很焦虑', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:19\",\"content\":\"非常符合\",\"title\":\"A\"},{\"index\":1,\"$$hashKey\":\"object:23\",\"title\":\"B\",\"content\":\"比较符合\"},{\"index\":2,\"$$hashKey\":\"object:27\",\"title\":\"C\",\"content\":\"比较不符合\"},{\"index\":3,\"$$hashKey\":\"object:31\",\"title\":\"D\",\"content\":\"完全不符合\"}]');
INSERT INTO `tb_question` VALUES ('5', '1', '你常常感到压力很大', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:13\",\"title\":\"A\",\"content\":\"比较符合\"},{\"index\":1,\"$$hashKey\":\"object:17\",\"title\":\"B\",\"content\":\"比较符合\"},{\"index\":2,\"$$hashKey\":\"object:21\",\"title\":\"C\",\"content\":\"比较不符合\"},{\"index\":3,\"$$hashKey\":\"object:25\",\"title\":\"D\",\"content\":\"完全不符合\"}]');
INSERT INTO `tb_question` VALUES ('6', '1', '我对未来怎么看？乐观还是悲观', 'subjective', null);
INSERT INTO `tb_question` VALUES ('7', '2', '植树节是哪一天？', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:7\",\"title\":\"A\",\"content\":\"3月12日\"},{\"index\":1,\"$$hashKey\":\"object:11\",\"title\":\"B\",\"content\":\"4月12日\"},{\"index\":2,\"$$hashKey\":\"object:15\",\"title\":\"C\",\"content\":\"3月13日\"}]');
INSERT INTO `tb_question` VALUES ('8', '2', '你对植树节了解多少', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:12\",\"title\":\"A\",\"content\":\"了解很多\"},{\"index\":1,\"$$hashKey\":\"object:16\",\"title\":\"B\",\"content\":\"了解一点\"},{\"index\":2,\"$$hashKey\":\"object:20\",\"title\":\"C\",\"content\":\"完全不了解\"}]');
INSERT INTO `tb_question` VALUES ('13', '2', '你觉得植树节有意义吗', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:17\",\"content\":\"很有意义\",\"title\":\"A\"},{\"index\":1,\"$$hashKey\":\"object:21\",\"title\":\"B\",\"content\":\"有些意义\"},{\"index\":2,\"$$hashKey\":\"object:25\",\"title\":\"C\",\"content\":\"毫无意义\"}]');
INSERT INTO `tb_question` VALUES ('15', '5', '你玩过游戏吗', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:7\",\"title\":\"A\",\"content\":\"玩过\"},{\"index\":1,\"$$hashKey\":\"object:11\",\"title\":\"B\",\"content\":\"没有\"}]');
INSERT INTO `tb_question` VALUES ('16', '5', '你每周花多少时间玩游戏？', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:11\",\"content\":\"1小时以下\",\"title\":\"A\"},{\"index\":1,\"$$hashKey\":\"object:15\",\"content\":\"10小时以下\",\"title\":\"B\"},{\"index\":2,\"$$hashKey\":\"object:19\",\"content\":\"30小时以下\",\"title\":\"C\"},{\"index\":3,\"$$hashKey\":\"object:23\",\"content\":\"50小时及以上\",\"title\":\"D\"}]');
INSERT INTO `tb_question` VALUES ('17', '5', '你觉得玩游戏有什么用？', 'subjective', null);
INSERT INTO `tb_question` VALUES ('18', '6', '你知道校园报警电话吗？', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:7\",\"title\":\"A\",\"content\":\"不知道\"},{\"index\":1,\"$$hashKey\":\"object:11\",\"title\":\"B\",\"content\":\"知道\"}]');
INSERT INTO `tb_question` VALUES ('19', '6', '你知道如何求救吗？', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:11\",\"title\":\"A\",\"content\":\"知道\"},{\"index\":1,\"$$hashKey\":\"object:15\",\"title\":\"B\",\"content\":\"不知道\"}]');
INSERT INTO `tb_question` VALUES ('20', '6', '你对学校保卫处有什么建议？', 'subjective', null);
INSERT INTO `tb_question` VALUES ('21', '7', '校外安全知识你知道吗？', 'objective', '[{\"index\":0,\"$$hashKey\":\"object:7\",\"title\":\"A\",\"content\":\"知道\"},{\"index\":1,\"$$hashKey\":\"object:11\",\"title\":\"B\",\"content\":\"不知道\"}]');
INSERT INTO `tb_question` VALUES ('22', '7', '你觉得学校应该做些什么？', 'subjective', null);

-- ----------------------------
-- Table structure for tb_subject
-- ----------------------------
DROP TABLE IF EXISTS `tb_subject`;
CREATE TABLE `tb_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `user_id` int(11) DEFAULT NULL COMMENT '关联用户表id',
  `content` varchar(1000) DEFAULT NULL COMMENT '问卷描述',
  `status` tinyint(1) DEFAULT NULL COMMENT '问卷状态，是否发布',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '问卷创建时间',
  `title` varchar(100) DEFAULT NULL COMMENT '问卷标题',
  `submit_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tb_subject_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='问卷表';

-- ----------------------------
-- Records of tb_subject
-- ----------------------------
INSERT INTO `tb_subject` VALUES ('1', '1', '您好,作为公司的中层管理者是中坚力量,为了对中层管理人员的培训更具有针对性和实用性,满足企业当前和预期的发展需求,切实帮助您更好地做好今后的工作。公司决定组织本次中层管理人员培训需求调查,真诚希望诸位能认真填写次调查问卷。谢谢您的配合!', '0', '2018-04-13 20:54:16', '运营主任级培训需求调查问卷', '0');
INSERT INTO `tb_subject` VALUES ('2', '1', '调查一下大家对植树节的了解程度', '1', '2018-04-13 20:54:19', '植树节调查', '0');
INSERT INTO `tb_subject` VALUES ('5', '9', '游戏调查问卷，游戏调查问卷', '1', '2018-04-16 21:39:03', '游戏调查问卷', '2');
INSERT INTO `tb_subject` VALUES ('6', '11', '防患于未然', '1', '2018-04-14 10:38:39', '校园安全', '2');
INSERT INTO `tb_subject` VALUES ('7', '11', '安全防护', '1', '2018-04-16 22:10:47', '校外安全', '2');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'cs1', '1111');
INSERT INTO `tb_user` VALUES ('2', '测试1', '1111');
INSERT INTO `tb_user` VALUES ('3', '测试2', '1111');
INSERT INTO `tb_user` VALUES ('8', '测试3', '1111');
INSERT INTO `tb_user` VALUES ('9', 'fengxing', '1111');
INSERT INTO `tb_user` VALUES ('10', 'fff', 'wwwff');
INSERT INTO `tb_user` VALUES ('11', 'www', 'www');
